// calculator.js
// This script is used for calculates two values.
var arr = new Array(); // for storing values in queue

function showscreen(opt)
{	
	'use strict';
	
	//Restrict the doubling of operator. 
	var op = document.getElementById('txtdisplay').value;
	var chkop = (((op.indexOf("+"))< 0) && ((op.indexOf("-"))< 0) && ((op.indexOf("*"))< 0) && ((op.indexOf("/"))< 0) && ((op.indexOf("%")< 0)));
  
	if (!chkop )
		{
			switch (opt) 
			{
				case '+':
					break;
				case '-':
					break;
				case '*':
					break;
				case '/':
					break;
				case '%':
					break;	
				//case '.':
					//break;						
				default:
					{
						if ((op.indexOf(".")< 0))
							{document.getElementById('txtdisplay').value += opt;}
							else
							{document.getElementById('txtdisplay').value += opt;}							
					}
			}
		}
			
	else if (op.length==0)
		{
			switch (opt) 
			{
				case '+':
					break;
				case '-':
					break;
				case '*':
					break;
				case '/':
					break;
				case '%':
					break;	
				//case '.':
					//break;						
				default:
					document.getElementById('txtdisplay').value += opt;
					rdisplay = true;
			}
		}
	else	
		return	document.getElementById('txtdisplay').value += opt; 
 }
 

function mcalc()
 {
	var fno=0;
	var sno=0;
	var optr="";
	var res=0;
	var op = 0;
			
			//	Clear the Table
		    document.getElementById("tbl").innerHTML = "";
            for (var i = 0; i < arr.length; i++) {
			op=arr[i];
			
				if ((op.indexOf("+"))> 0)
					//optr = "+"; 
					{fno = op.slice(0,op.indexOf("+"));
					sno = op.slice((op.indexOf("+")+1), op.length);	
					res = (+fno) + (+sno);} //use uniary operator to convert string into number
					
				else if ((op.indexOf("-"))> 0)
					{fno = op.substring(0, op.indexOf("-"));
					sno = op.substring((op.indexOf("-")+1), op.length);					
					res = fno - sno;					
					}
				else if( (op.indexOf("*")) > 0)
					{optr = "*";
					fno = op.substring(0, op.indexOf("*"));
					sno = op.substring((op.indexOf("*")+1), op.length);								
					res = fno * sno;					
					}
				else if( (op.indexOf("/"))> 0)
					{optr = "/";
					fno = op.substring(0, op.indexOf("/"));
					sno = op.substring((op.indexOf("/")+1), op.length);										
						if(fno == 0){res="Error"} else res = fno / sno;					
					}
				else if(( op.indexOf("%"))> 0)
					{optr = "%";
					fno = op.substring(0, op.indexOf("%"));
					sno = op.substring((op.indexOf("%")+1), op.length);										
					res = fno % sno;					
					}
					
					document.getElementById("tbl").innerHTML += "<tr><td>" + arr[i] + "</td><td> = " + res + "</td></tr>";
	}
	if (arr.length==0)
		{document.getElementById('txtdisplay').value = "No Value in Queue";}
	else
		{document.getElementById('txtdisplay').value = "";}
	return false;
	
 }
 
 //Function for storing values in Queue
 function setq()
		{
            var qvalue = document.getElementById("txtdisplay").value;
            arr[arr.length] = qvalue;
			
            document.getElementById("tbl").innerHTML = "";
            for (var i = 0; i < arr.length; i++) {
                document.getElementById("tbl").innerHTML += "<tr><td>" + arr[i] + "</td></tr>";
            }
            	//Clear Display Screen
				document.getElementById('txtdisplay').value = "";            
}

//Clear Values
function clearall()
{
		document.getElementById('txtdisplay').value = "";            
		//For clear the Array and Que Table
		//arr.length = 0;
        //document.getElementById("tbl").innerHTML = "";
}

