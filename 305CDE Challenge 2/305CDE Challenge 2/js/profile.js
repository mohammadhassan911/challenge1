// profile.js
// This script creates user profile.
var uuid = 0;
var totalage = 0;

function profile() {
    'use strict';

    //get value from the profile form
    var username = document.getElementById('txtusername').value;
    var name = document.getElementById('txtname').value;
    var email = document.getElementById('email').value;
    var password = document.getElementById('txtpassword').value;
    var age = document.getElementById('txtage').value;

    var display = document.getElementById('tbldisplay'); // to display the value

    uuid++;
    totalage += (+age);

    var person = {
        username: username,
        name: name,
        email: email,
        password: password,
        age: age,
        idno: uuid
    };

    console.log(person);

    // Create the ouptut for HTML Table:
    var displaymsg = '<tr><td>' + person.idno + '<td>';
        displaymsg += '<td>' + person.username + '<td>';
        displaymsg += '<td>' + person.name + '</td>';
        displaymsg += '<td>' + person.email + '</td>';
        displaymsg += '<td>' + person.password + '</td>';
        displaymsg += '<td>' + person.age + '</td>';
        
    
        document.getElementById("tbldisplay").innerHTML += displaymsg;

    return false;

}

function avgage()
    {
    alert("Averge age is :" + totalage / uuid);
    }

// Initial setup:
function init() {
    'use strict';
    document.getElementById('theForm').onsubmit = profile;
} // End of init() function.
window.onload = init;
