// JavaScript source code
// employee.js
// This script creates an object using form data.

// Function called when the form is submitted.
// Function creates a new object.

var uuid = 0;
var totalage = 0;
var pvalue = new Array();

function profile() {
    'use strict';

    //get value from the profile form
    var username = document.getElementById('txtusername').value;
    var name = document.getElementById('txtname').value;
    var email = document.getElementById('email').value;
    var password = document.getElementById('txtpassword').value;
    var age = document.getElementById('txtage').value
    var display = document.getElementById('tbldisplay'); // to display the value

    uuid++;
    totalage += (+age);

    var person = {
        username: username,
        name: name,
        email: email,
        password: password,
        age: age,
        idno: uuid
    };

    //console.log(display);

    // Create the ouptut as HTML:
    var displaymsg = '<tr><td>' +person.username+ '<td>';
    displaymsg += '<td>' + person.name + '</td>';
    displaymsg += '<td>' + person.email + '</td>';
    displaymsg += '<td>' + person.password + '</td>';
    displaymsg += '<td>' + person.age + '</td>';
    displaymsg += '<td>' + person.idno + '</td></tr>';
    
    //document.getElementById("tbldisplay").innerHTML += person;
    pvalue[uuid] = person;
    setq();

    // Return false:
    return false;

}

//Function for storing values in Queue
function setq() {
   // var qvalue = document.getElementById("txtdisplay").value;
    //arr[arr.length] = qvalue;

    document.getElementById("tbldisplay").innerHTML = "";
    for (var i = 0; i < pvalue.length; i++) {
        document.getElementById("tbldisplay").innerHTML += "<tr><td>" + pvalue[i] + "</td></tr>";
    }
    //Clear Display Screen
    //document.getElementById('txtdisplay').value = "";
}


function avgage()
    {
    alert("Averge age is :" + totalage / uuid);
    }


// End of process() function.
// Initial setup:
function init() {
    'use strict';
    document.getElementById('theForm').onsubmit = profile;
} // End of init() function.
window.onload = init;
